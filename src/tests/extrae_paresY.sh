#!/bin/bash

##	extrae_pares
#
#	Conjunción Y.

# DEPENDENCIAS:
#	extrae_paresT.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"

cd ..

function llama_perl {
	archivo_entrada="$1"
	perl -C -- src/extrae_paresY.pl "$archivo_entrada"
}
export -f llama_perl

function main {
	if [[ $flag_split == true ]]; then parallel llama_perl ::: "corpus/split_${prefijo_archivo}_out"/* 
	else llama_perl "corpus/${prefijo_archivo}_out" 
	fi > "out/pares_asociacionY/temp_salida"
	sort "out/pares_asociacionY/temp_salida" | uniq -c | sort -rn \
	 > "out/pares_asociacionY/respuestas"
	rm "out/pares_asociacionY/temp_salida"
	primer1=$(grep -wnm 1 "1" "out/pares_asociacionY/respuestas" | awk -F ":" '{print $1}')
	if [[ "$primer1" -gt 1 ]]; then
		(( primer1-- ))
		head -n "$primer1" "out/pares_asociacionY/respuestas" > "out/pares_asociacionY/temp_salida"
		mv "out/pares_asociacionY/temp_salida" "out/pares_asociacionY/respuestas"
	fi
}

if [[ ! -d "out/pares_asociacionY/" ]];then
	mkdir -p "out/pares_asociacionY/"
fi

main
