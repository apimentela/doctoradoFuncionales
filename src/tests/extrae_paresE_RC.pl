#!/usr/bin/perl

##	extrae_pares
#	Este programa tiene el propósito de obtener pares de palabras
#	relacionadas

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $palabra_estimulo = '\S+';
my $archivo_entrada = $ARGV[0];

# para obtener las listas, me estoy quedando con aquellas palabras de la
#	izquierda que tienen más de una palabra de la derecha asociada, una
#	vez que las encuentro, le sumo las palabras de la lista extra, y para
#	las palabras de la derecha me quedo con todas las asociadas a las de
#	la izquierda.

my $lista_funcs_1 = 'the|a|his|one|two|her|new|their|its|he|she|they|be';
my $lista_funcs_2 = 'on|and|by|about|or|that|for|to|at|as|is|in|of|with|was|are';


my $verificacion = '\b(?:' . $lista_funcs_1 . '|' . $lista_funcs_2 . ')\b';
my $conectora = '\bof\b';
my $sin_conectora = 'the|a|his|one|two|her|new|their|its|he|she|they|be|on|and|by|about|or|that|for|to|at|as|is|in|with|was|are';

my $ignorar = '(DIGITO|[^\w\s])';

my $expresion_1 = '\b(\S+) (' . $palabra_estimulo . ')(?= (\S+) \1 (\S+)(?: (?:'. $verificacion .')\b| [^\w\s]|$))'; # Esta expresión incluye signos de puntuacion

open(my $fhc, '>', "${archivo_entrada}_conect") or die "No se pudo abrir para escribir, $!";
#open(my $fh, '>', "${archivo_entrada}_multi") or die "No se pudo abrir para escribir, $!";
open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion_1/g){
		my $func1 = $1;
		my $func2 = $5;
		my $palabra1 = $2;
		my $palabra2 = $4;
		my $palabra_mid = $3;
		if ( $func1 =~ /$ignorar/ || $palabra2 =~ /$verificacion/ || $palabra2 =~ /$ignorar/ || $palabra1 =~ /$verificacion/ || $palabra1 =~ /$ignorar/ || $palabra2 eq $palabra1 ) { next; }
		if ( $palabra_mid =~ /$conectora/ ){
			if ( $func2 =~ /[^\w\s]/ ) { next; }
			$palabra1 =~ s/ /_/g;
			$palabra2 =~ s/ /_/g;
			print $fhc "$palabra1 $func1 $palabra2 $func2\n";
		}
		else{# ( $palabra_mid =~ /$verificacion/ ){
			$palabra1 =~ s/ /_/g;
			$palabra2 =~ s/ /_/g;
			print "$palabra1 $palabra_mid $palabra2\n";
		}
		#else {
		#	$palabra1 =~ s/ /_/g;
		#	print $fh "$palabra1 $palabra_mid\n";
		#}
	}
}
close INPUT;
#close $fh;
close $fhc;
