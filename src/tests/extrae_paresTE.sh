#!/bin/bash

##	extrae_pares
#
#	Este programa trabaja en conjunto con el de perl para extraer y filtrar
#	pares de palabras relacionadas con una lista de entrada.

# DEPENDENCIAS:
#	extrae_paresT.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"
export carpeta_salida="out/pares_asociacionTE/"

cd ..

function llama_perl {
	archivo_entrada="$1"
	perl -C -- src/extrae_paresTE.pl "$archivo_entrada"
}
export -f llama_perl

function main {
	if [[ $flag_split == true ]]; then parallel llama_perl ::: "corpus/split_${prefijo_archivo}_out"/* 
	else llama_perl "corpus/${prefijo_archivo}_out" 
	fi > "${carpeta_salida}temp_salida"
	sort "${carpeta_salida}temp_salida" | uniq -c | sort -rn \
	 > "${carpeta_salida}respuestas"
	rm "${carpeta_salida}temp_salida"
	primer1=$(grep -wnm 1 "1" "${carpeta_salida}respuestas" | awk -F ":" '{print $1}')
	if [[ "$primer1" -gt 1 ]]; then
		(( primer1-- ))
		head -n "$primer1" "${carpeta_salida}respuestas" > "${carpeta_salida}temp_salida"
		mv "${carpeta_salida}temp_salida" "${carpeta_salida}respuestas"
	fi
}

if [[ ! -d "${carpeta_salida}" ]];then
	mkdir -p "${carpeta_salida}"
fi

main
