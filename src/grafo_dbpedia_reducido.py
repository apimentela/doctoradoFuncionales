#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Este programa es el encargado de convertir las tripletas en grafos mas simples
# La información se puede obtener de: https://wiki.dbpedia.org/Downloads2014
# Creo que la más rica estan en la sección 4: Localized datasets
# para obtener la versión reducida, se pretende tomar en cuenta solo los links que se ofrecen
#	por la dbpedia en lugar de sacarlos de las palabras de los abstracts
# de allí que los archivos para obtener la información serán los Titles (labels)
# 	y los artcles-categories
# estos últimos también tienen sus propios labels

# Se tienen que seguir los siguientes pasos:
#	- Recorrer el archivo de categorias
#	- identificar el resource y la categoría
#	- ir al archivo de labels para encontrar el label de resource
#	- ir al archivo de category-labels para encontrar el label de la categoría
#	- armar el grafo con los pares de labels

import re
import os

def main(args):
	nombre_labels = "corpus/labels_es.ttl"
	nombre_category_labels = "corpus/category_labels_es.ttl"
	nombre_categories = "corpus/article_categories_es.ttl"
	
	re_resource = re.compile(r"^\S+")
	re_category = re.compile(r" (\S+) .$")
	re_resource_label = re.compile(r"> \"([^\(\"]+)")
	re_category_label = re.compile(r"> \"([^\(\"]+)")
	
	# Para que el proceso no sea eterno, debo crear un diccionario que vaya de las
	#	etiquetas hacia las labels
	titulos={}
	category_labels={}
	with open(nombre_labels) as archivo_labels:
		for linea_labels in archivo_labels:
			try:
				key = re_resource.search(linea_labels).group(0)
				titulo = re_resource_label.search(linea_labels).group(1)
				titulo = titulo.strip().lower()
				titulo = titulo.replace(" ","_")
				titulos[key] = titulo
			except: pass
	with open(nombre_category_labels) as archivo_labels:
		for linea_labels in archivo_labels:
			try:
				key = re_resource.search(linea_labels).group(0)
				category_label = re_category_label.search(linea_labels).group(1)
				category_label = category_label.strip().lower()
				category_label = category_label.replace(" ","_")
				category_labels[key] = category_label
			except: pass	
	
	# Este es el loop principal sobre las categorías
	with open(nombre_categories) as archivo:
		for linea in archivo:

			try:
				resource = re_resource.search(linea).group(0)
				category = re_category.search(linea).group(1)
			except:
				continue
			
			# Con ambos identificadores, se buscan en el diccionario para obtener el par de labels
			try:
				titulo = titulos[resource]
				category_label = category_labels[category]
			except:
				continue
							
			print(titulo,category_label,sep='\t')

	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
