#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from nltk.tokenize import TweetTokenizer

def main(args):
	nombre_archivo = args[1]
	
	with open(nombre_archivo) as archivo:
		tweet_tokenizer = TweetTokenizer()
		for linea in archivo:
			try:
				datos = json.loads(linea)
				if datos["lang"] == "en":
					if datos["truncated"]:
						texto = datos["extended_tweet"]["full_text"]
					else:
						texto = datos["text"]
					tokens = tweet_tokenizer.tokenize(texto)
					texto = " ".join(tokens)
					print(texto)
			except:
				pass
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
