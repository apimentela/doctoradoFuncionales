#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Este programa es el encargado de convertir las tripletas en grafos mas simples
# La información se puede obtener de: https://wiki.dbpedia.org/Downloads2014
# Creo que la más rica estan en la sección 4: Localized datasets
# de allí, los mas importantes parecen ser los Titles y los Long abstracts en formato TTL

# Se puede contar con un catalogo ya pre-procesado de labels si se usa la siguiente instrucción:
# grep -Po "> \"\K[^\(\"]*" labels_es.ttl | perl -C -ne 'print lc' | sort -u > labels.txt

# Se tienen que seguir los siguientes pasos:
#	- Por un lado, obtener todas las labels del archivo: "labels_es.ttl"
#		se trata de labels multipalabra, así que se deben unir con '_' pero tampoco se debe perder la información de su resource (primer entrada de la tripleta)
#		a información del resource es la que va a ligar cada label con las palabras de su "long_abstracts_es.ttl", lo cual va al paso 2
#	- Reemplazar en cada uno de los long abstracts, las apariciones de labels que se encuentren por una sola entidad.
#	- Armar un grafo relacionando cada entidad (label) con todas las que se hayan encontrado en su long abstract.

import re
import os

def main(args):
	# ~ nombre_archivo = args[1]
	# ~ nombre_labels = args[2]
	
	nombre_archivo = "corpus/long_abstracts_es.ttl"
	nombre_labels = "corpus/labels_es.ttl"
	
	re_resource = re.compile(r"^\S+")
	re_abstract = re.compile(r".*?\"(.*)(?=\".*?$)")
	re_titulo = re.compile(r"> \"([^\(\"]+)")
	
	carpeta = os.path.dirname(nombre_archivo) ## directory of file
	
	# Aqui uso el archivo auxiliar parar tener un catalogo de labels
	labels = set([])
	with open(carpeta+"/labels.txt") as archivo_labels:
		for linea in archivo_labels:
			label = linea.strip()
			# Reviso unas cuantas cosas para filtrar los nodos a usar
			if re.match(r"^(\d|[^\w])+$",label):	continue
			if len(label) < 4: continue
			labels.add(label)
	labels = sorted(labels,key=len,reverse=True)
	
	# Y para tener también a la mano su versión con guion en lugar de espacio
	labels_guion = []
	for label in labels:
		label_guion = label.replace(" ","_")
		labels_guion.append(label_guion)
		
	# Para que el proceso no sea eterno, debo crear un diccionario que vaya de las
	#	etiquetas hacia las labels
	titulos={}
	with open(nombre_labels) as archivo_labels:
		for linea_labels in archivo_labels:
			try:
				key = re_resource.search(linea_labels).group(0)
				titulo = re_titulo.search(linea_labels).group(1)
				titulo = titulo.strip().lower()
				titulo = titulo.replace(" ","_")
				titulos[key] = titulo
			except: pass
	
	# Este es el loop principal sobre los abstracts
	with open(nombre_archivo) as archivo:
		for linea in archivo:
			error = False
			try:
				resource = re_resource.search(linea).group(0)
				abstract = re_abstract.search(linea).group(1)
			except:
				continue
			
			# Si ya tengo el resource, me conviene obtener el label de una vez, le llamaré titulo para evitar ambiguedad
			try:
				titulo = titulos[resource]
			except:
				continue
			
			# Hay que trabajar sobre el abstract para reemplazar todas las apariciones de labels multipalabra a una sola
			# 	además, se puede aprovechar el mismo recorrido para hacer las ligas con el label principal
			abstract = abstract.lower()
			for i,label in enumerate(labels):
				if label in abstract:
					try:
						if re.search(r"\b"+re.escape(labels[i])+r"\b",abstract):
							print(titulo,labels_guion[i],sep='\t')
						
						# Reemplazar el texto en el abstract me ayudará a que no haya coincidencias extra
						#	con los elementos posteriores
						abstract = re.sub(r"\b"+re.escape(labels[i])+r"\b",labels_guion[i],abstract)
					except:
						pass
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
