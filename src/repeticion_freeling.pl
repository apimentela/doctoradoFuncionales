#!/usr/bin/perl

##	extrae_pares
#	Este programa tiene el propósito de obtener pares de palabras
#	relacionadas

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $palabra_estimulo = '\S+';
my $archivo_entrada = $ARGV[0];

my $lista_funcs_1 = 'la|el|los|las|del|su|al|un|se|the|san|una|lo|n|sus|dos';
my $lista_funcs_2 = 'de|y|en|a|con|es|por|para|hasta|of|fue|o|que|durante|sobre|contra|como';

my $ignorar = '(DIGITO|[^\w\s])';

my $expresion_1 = '\b(.+)(?:(?:' . $lista_funcs_1 . ') )? (' . $palabra_estimulo . ') (\S+) \1 (?:(?:' . $lista_funcs_1 . ') )?(\S+)\b';

my $verificacion = '\b(?:' . $lista_funcs_1 . '|' . $lista_funcs_2 . ')\b';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion_1/g){
		if ( $4 =~ /$verificacion/ || $2 =~ /$verificacion/ || $& =~ /$ignorar/ || $2 eq $4 ) { next; }
		print "$2 $3 $4\n";
	}
}
close INPUT;
