#!/usr/bin/perl

##	repeticion_freeling
#	Este programa tiene el propósito de obtener la serie de patrones POS
#	de repetición a partir de un corpus que ya haya sido etiquetado por freeling

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

#~ my $archivo_entrada = $ARGV[0];

# D : determinantes
# P : pronombres
# C : conjunciones
# S : preposiciones

# V : verbos
# N : nombres
# R : adverbios
# A : adjetivos


my $patron_funcional = '[DPCS]\S\n';
my $patron_contenido = '[VNRA]\S\n';

my $patron_repeticion = '((?:' . $patron_funcional . ')+)' . '((?:' . $patron_contenido . ')+)' . '((?:' . $patron_funcional . ')+)' . '\1' . '\2' . '(?=[DPCSF]\S\n|\n|$)';


#~ open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
#~ while(<INPUT>){
while(<>){
	#~ print "$_\n";
	while ($_ =~ /$patron_repeticion/g){
		my $salida = $&;
		$salida =~ tr/\n/ /;
		print "$salida\n";
	}
}
#~ close INPUT;
