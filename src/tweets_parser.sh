#!/bin/bash

##	tweets_parser
#
#	Este programa se encarga de extraer el texto de tweets
#	descargados en el formato json
# DEPENDENCIA: tweets_parser.py

nombre_programa="$BASH_SOURCE"
ruta=$(realpath "$BASH_SOURCE")
cd "${ruta%/*}" || exit # AQUI COMIENZA EL PROGRAMA
ruta=$(realpath ..)
corpus="corpus" #TODO: dar opcion


# Default behavior
# DE MOMENTO LAS OPCIONES NO HACEN NADA
flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export nombre_entrada="$1"

cd "$ruta"

########################################################################

function parser {
	archivo_entrada="$1"
	python3 src/tweets_parser.py "$archivo_entrada"
}
export -f parser

find "$corpus/$nombre_entrada" -type f > temp

parallel --bar parser :::: temp > "$corpus/${nombre_entrada}_out"
