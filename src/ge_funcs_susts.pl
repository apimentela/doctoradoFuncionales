#!/usr/bin/perl

##	ge_funcs_susts
#	Este programa tiene el propósito de obtener patrones de repetición

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $archivo_entrada = $ARGV[0];

# para obtener las listas, me estoy quedando con aquellas palabras de la
#	izquierda que tienen más de una palabra de la derecha asociada, una
#	vez que las encuentro, le sumo las palabras de la lista extra, y para
#	las palabras de la derecha me quedo con todas las asociadas a las de
#	la izquierda.

my $expresion = '\b(\S+) (?=(\S+) (\S+) \1 \S+\b)'; # Esta expresión incluye signos de puntuacion

my $ignorar = 'DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion/g){
		my $func1 = $1;
		my $func2 = $3;
		my $contenido = $2;
		
		if ( $contenido =~ /$ignorar/ || $contenido eq $func1 || $contenido eq $func2 ) { next; }
		
		print "$func2 $func1\n"; # Es importante imprimirlo en orden inverso para el siguiente proceso
	}
}
close INPUT;
