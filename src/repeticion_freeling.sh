#!/bin/bash

##	repeticion_freeling
#
#	Este programa tiene el propósito de extraer los pares de palabras que
#	cumplan con algún patrón de repetición POS a partir de un corpus que
#	esté previamente etiquetado con freeling.

# DEPENDENCIAS:
#	patrones_repeticion_freeling.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"

cd ..

function extrae_patrones {
	archivo_entrada="$1"
	awk '{print substr($3,1,2)}' "$archivo_entrada" |
	  perl -C -w0 src/patrones_repeticion_freeling.pl
}
export -f extrae_patrones

function extrae_pares {
	patron="$1"
	patron_unido=$(echo "$patron" | tr " " "_")
	echo "procesando $patron_unido"
	if [[ $flag_split == true ]]; then parallel perl -C -w0 src/pares_repeticion_freeling.pl ::: "$patron" ::: "out/split_${prefijo_archivo}_tagged"/* 
	else perl -C -w0 src/pares_repeticion_freeling.pl "$patron" "out/${prefijo_archivo}_tagged" 
	fi > "out/${prefijo_archivo}_pares/$patron_unido"
}
export -f extrae_pares

function ordenamiento {
	archivo="$1"
	sort "$archivo" | uniq -c | sort -rn > "${archivo}_temp"
	mv "${archivo}_temp" "$archivo" 
	#~ primer1=$(grep -wnm 1 "^ *1" "$archivo" | awk -F ":" '{print $1}')
	#~ if [[ "$primer1" -gt 1 ]]; then
		#~ (( primer1-- ))
		#~ head -n "$primer1" "$archivo" > "${archivo}_temp"
		#~ mv "${archivo}_temp" "$archivo"
	#~ fi
}
export -f ordenamiento

if [[ $flag_split == true ]]; then
	#~ cp -r "out/split_${prefijo_archivo}_tagged" "out/temp"
	parallel extrae_patrones ::: "out/split_${prefijo_archivo}_tagged"/* 
else
	#~ cp "out/${prefijo_archivo}_tagged" "out/temp"
	extrae_patrones "out/${prefijo_archivo}_tagged" 
fi | sort -u | sed '/^$/d' |
  awk '{printf("%s:%s\n",NF,$0)}'  | sort -n | awk -F ":" '{print $2}' \
  > 'temp_patrones'

if [[ ! -d "out/${prefijo_archivo}_pares" ]]; then
	mkdir "out/${prefijo_archivo}_pares"
fi

while read patron; do
	extrae_pares "$patron"
done < "temp_patrones"

rm "temp_patrones"

parallel ordenamiento ::: "out/${prefijo_archivo}_pares"/*
