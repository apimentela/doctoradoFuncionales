#!/usr/bin/perl

##	repeticion_freeling
#	Este programa tiene el propósito de obtener la serie de patrones POS
#	de repetición a partir de un corpus que ya haya sido etiquetado por freeling

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use File::Slurp::Unicode;
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $patron = $ARGV[0]; # El patron entra como una línea de pares de letras POS gruesas separadas por espacios
$patron =~ s/^\s+|\s+$//g ;
#~ my $logpath = shift; # se quita el parametro de entrada para vaciar ARGV y que lea STDIN
my $archivo_entrada = $ARGV[1];

# D : determinantes
# P : pronombres
# C : conjunciones
# S : preposiciones

# F : puntuacion

# V : verbos
# N : nombres
# R : adverbios
# A : adjetivos

my @elementos_patron = split(/ /, $patron); # Para poder procesar facilmente las etiquetas por separado, las separo en una lista
my $patron_repeticion = '';	# inicializo el patrón de repetición que se buscará al final
my $b_contenido_encontrada = 0;	# inicializo unas variables booleanas para localizar las palabras de contenido dentro del patron
my $b_funcional_encontrada = 0;
my $b_segunda_contenido = 0;
foreach my $elemento (@elementos_patron){	# hago un recorrido de mis etiquetas del patrón
	my $pos_principal = substr($elemento,0,1);	# y para cada una, obtengo la primera letra, que es el POS principal
	if (! $b_contenido_encontrada ){	# si aun no he encuentrado ninguna palabra de contenido...
		if ($pos_principal =~ /[VNRA]/){	# reviso para ver si la que me acabo de topar es de contenido...
			$b_contenido_encontrada = 1; # si si, entonces prendo la bandera, 
			$patron_repeticion .= '(\S+ \S+ ' . $elemento . '\S* \S+\n'; # y agrego un paréntesis de captura al patrón
		}
		else { # si no...
			$patron_repeticion .= '\S+ \S+ ' . $elemento . '\S* \S+\n'; # solo agrego esta linea al patrón sin más
		}
	}
	elsif (! $b_funcional_encontrada){ # si ya encontré una palabra de contenido, entonces tengo que revisar si ya volví a encontrar otra funcional...
		if ($pos_principal =~ /[DPCS]/){ # si aun no, tengo que revisar si la que estoy analizando ya lo es...
			$b_funcional_encontrada = 1; # si sí, entonces prendo la bandera de funcional encontrada,
			$patron_repeticion .= ')\S+ \S+ ' . $elemento . '\S* \S+\n'; # y cierro el paréntesis de captura del patrón
		}
		else { # si no...
			$patron_repeticion .= '\S+ \S+ ' . $elemento . '\S* \S+\n'; # de nuevo solo agrego la linea al patrón sin más
		}
	}
	elsif (! $b_segunda_contenido) { # por ultimo hay que revisar si no se ha encontró la segunda funcional
		if ($pos_principal =~ /[VNRA]/){	# reviso para ver si la que me acabo de topar es de contenido...
			$b_segunda_contenido = 1; # si si, entonces prendo la bandera, 
			$patron_repeticion .= '(?=(\S+ \S+ ' . $elemento . '\S* \S+\n'; # y agrego otro paréntesis de captura al patrón
		}
		else { # si no...
			$patron_repeticion .= '\S+ \S+ ' . $elemento . '\S* \S+\n'; # solo agrego esta linea al patrón sin más
		}
	}
	else{ # ya que encontré todo
		$patron_repeticion .= '\S+ \S+ ' . $elemento . '\S* \S+\n'; # solo agrego las lineas al patrón sin más
	}
}
$patron_repeticion .= ')(?:\S+ \S+ [DPCSF]|\n|$))'; # por útlimo cierro el segundo paréntesis de captura y me aseguro de que venga una palabra funcional o una línea vacía o fin del documento después.


open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$patron_repeticion/g){
		my $match1 = $1;
		my $match2 = $2;
		
		my @palabras1 = ();
		my @lineas1 = split(/\n/,$match1);
		foreach my $linea (@lineas1){
			my $palabra = (split(/ /,$linea))[1];
			push(@palabras1,$palabra);
		}
		my $palabras1 = join('_',@palabras1);
		
		my @palabras2 = ();
		my @lineas2 = split(/\n/,$match2);
		foreach my $linea (@lineas2){
			my $palabra = (split(/ /,$linea))[1];
			push(@palabras2,$palabra);
		}
		my $palabras2 = join('_',@palabras2);
		
		my $salida = "$palabras1 $palabras2";
		print "$salida\n";
	}
}
close(INPUT);

#~ my $texto = read_file($archivo_entrada);
#~ $texto =~ s/$patron_repeticion//g;
#~ open(my $fh, '>', "$archivo_entrada") or die "No se pudo abrir para escribir, $!";
#~ print $fh "$texto";
#~ close $fh;
