#!/usr/bin/perl

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $archivo_entrada = $ARGV[0];

my $lista = 'yet|with respect to|with regard tobehind|withoutbefore|withinbecause of|withat |while|wheter|wherever|where|whenever|when|up toaround|uponamong|upalong|untilagainst|until|unless|underneathafter|underacross|towardabove|to|till|throughout|through|though|the|that|than|so that|some|so|since|since|regarding|provided that|past|over|outside|out|or|onto|only if|one|once|on|off|of|not only|nor|neither|near|like|lest|into|instead of|in spite of|inside|in order than|in front of|in case|in|if|from|for|few|excepting|except|even though|even if|either|during|down|despite|concerning|by the time|by|but also|but|but|both|beyond|between|besides|beside|beneath|below|before|because|as though|as soon as|as much as|as long as|as if|as|any|and|an|although|also|after|about|a';

#my $expresion = '\b((?:' . $lista .') \S+) (?=((?:' . $lista . ') \S+ (?:' . $lista . '))\b)';
my $expresion = '\b((?:(?:' . $lista . ')\b *?)+) ((?:(?:\S+) *?)+?) (?=((?:(?:' . $lista . ')\b *?)+) ((?:(?:\S+) *?)+?) ((?:(?:' . $lista . ')\b *?)+)\b)';

my $verificacion = '\b(?:' . $lista . ')\b|DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion/g){
		my $primero = $1;
		my $segundo = $2;
		my $tercero = $3;
		my $cuarto = $4;
		my $quinto = $5;
		if ( $segundo =~ /$verificacion/ or $cuarto =~ /$verificacion/ or $segundo eq $cuarto ) { next; }
		$primero =~ s/ /_/g;
		$segundo =~ s/ /_/g;
		$tercero =~ s/ /_/g;
		$cuarto =~ s/ /_/g;
		$quinto =~ s/ /_/g;
		print "$primero $segundo $tercero $cuarto $quinto\n";	
	}	
}
close INPUT;

