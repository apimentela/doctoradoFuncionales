#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa trabaja con el grafo que se forma de un diccionario y con los
# grafos de parejas de oposición, el objetivo es recortar el grafo de
# diccionarios para obtener uno en el que se tengan solamente los términos mas
# importantes para la tarea de las definiciones onomasilogicas

def main(args):
    identificador = args[1]
    
    num_lineas = 0
    vocabulario = set()
    with open("grafo"+identificador+".tsv") as archivo:
        for linea in archivo:
            terminos = linea.split()
            vocabulario.add(terminos[0])
            vocabulario.add(terminos[1])
            num_lineas += 1
    
    lista_terminos = [""] # Los valores que quiero en la matriz son numéricos, para darle ID a las palabras puedo usar su lugar en una lista (el primer elemento lo dejo vacío para que comience en 1
    lista_terminos += sorted(vocabulario)

    diccionario = {}
    for i,termino in enumerate(lista_terminos):
        diccionario[termino] = i

    nodos = len(vocabulario)
    print("%%MatrixMarket matrix coordinate pattern symmetric")
    print(nodos,nodos,num_lineas)
    with open("grafo"+identificador+".tsv") as archivo:
        for linea in archivo:
            terminos = linea.split()
            ID1 = diccionario[terminos[0]]
            ID2 = diccionario[terminos[1]]
            print(ID1,ID2)

    # ~ with open('indiceGrafos.pickle','w') as pkfile:
        # ~ pickle.dump(indice,pkfile)

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
