#!/bin/bash

identificador="$1"
distancia="${2:-1}"

parallel --line-buffer --will-cite python recortaGrafosParallel.py ::: "$identificador" ::: "$distancia" ::: cliques1/*
