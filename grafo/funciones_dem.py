#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa trabaja con el diccionario DEM
# se asume que el excel con el contenido esta en la misma carpeta

# Este programa asume que existe un servidor de freeling funcionando, para lograrlo es necesario instalarlo y correr un comando como este:
#   analyze --server --port 9191 -f /usr/share/freeling/config/es.cfg --flush --output json
#   analyze --server --port 9191 -f /usr/local/share/freeling/config/es.cfg --flush --output json
# La segunda opción es para mac

import re
import json
import xlrd
import subprocess
from nltk.corpus import stopwords

def main(args):
    conexiones = {}
    puerto = "9191"
    fname = 'DEM.xls'
    re_etiqueta = re.compile("<[^>]*>")
    palabras_funcionales = stopwords.words("spanish")
    xl_workbook = xlrd.open_workbook(fname)
    xl_sheet = xl_workbook.sheet_by_index(0)
    num_cols = xl_sheet.ncols   
    palabra_pre = ''
    ponderador = 1.0
    for row_idx in range(1, xl_sheet.nrows):
        palabra = str(xl_sheet.cell(row_idx,0).value)
        if palabra in palabras_funcionales or not palabra.isalpha(): continue
        if palabra not in conexiones:
            conexiones[palabra] = {}
        
        definicion = xl_sheet.cell(row_idx,10).value
        definicion = re_etiqueta.sub("",definicion)
        if definicion == 'NULL': continue
        salida_freeling = subprocess.check_output(('analyzer_client',puerto),input=definicion.encode()) # El enconde es necesario
        try:
            info_freeling = json.loads(salida_freeling)
        except: continue
        if palabra == palabra_pre:
            ponderador += 1.0
        else:
            ponderador = 1.0
        palabra_pre = palabra
        ponderacion = 1.0/ponderador
        for oracion in info_freeling["sentences"]:
            for token in oracion["tokens"]:
                lemma = token["lemma"]
                if lemma in palabras_funcionales or not lemma.isalpha(): continue
                conexiones[palabra][lemma]=ponderacion
    
    with open("grafo_diccionarioDEM.tsv",'w') as archivo_salida:
        for i in conexiones:
            for j in conexiones[i]:
                archivo_salida.write(" ".join([i,j,str(conexiones[i][j])])+"\n")
    
        # ~ if concepto != xl_sheet.cell(row_idx,0).value:
            # ~ if concepto!= '':#Es el primero
                # ~ dic[concepto] = lista_defs          
                # ~ lista_defs = []
            # ~ concepto = xl_sheet.cell(row_idx,0).value           
            # ~ if xl_sheet.cell(row_idx,4).value == 'NULL' and xl_sheet.cell(row_idx,10).value != 'NULL':
                # ~ lista_defs.append(xl_sheet.cell(row_idx,10).value)
            # ~ continue
        # ~ else:
            # ~ if xl_sheet.cell(row_idx,4).value == 'NULL' and xl_sheet.cell(row_idx,10).value != 'NULL':
                # ~ lista_defs.append(xl_sheet.cell(row_idx,10).value)
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
