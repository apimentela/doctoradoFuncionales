#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa trabaja con el grafo que se forma de un diccionario y con los
# grafos de parejas de oposición, el objetivo es recortar el grafo de
# diccionarios para obtener uno en el que se tengan solamente los términos mas
# importantes para la tarea de las definiciones onomasilogicas

import networkx as nx
import pickle

class indiceGrafos:
    def __init__(self,G_dict,G_ants):
        self.G_dict = G_dict # Lo primero es tener el grafo del diccionario y antónimos, estos se recibe en la creación
        self.G_ants = G_ants
        self.grafos = []    # los grafos se pueden identificar por su índice en esta lista
        self.contenido = [] # Para lograr el índice, primero tendre el "contenido" que es dado el grafo, los scores de sus términos
        self.indice = {}    # Este es el objetivo, un diccionario que dado el termino, nos indique sus mejores grafos
        self.ranking = {}    # Además del índice como tal, un ranking que me indique simplemente una lista con los grafos ordenados de mayor a menor importancia puede ser de mas utilidad
        self.ready = False  # Iré llenando los grafos y el contenido poco a poco, asi que tendré esta bandera para alzarla luego de hacer la conversión al índice

    def armaContenido(self,distancia=1,centralidad=nx.algorithms.betweenness_centrality):
        # Esta función toma el grafo de antónimos, lo recorre en busca
        # de cliques, y toma dichos cliques como base para formar los
        # grafos que finalmente se agregarán en el objeto, estos grafos
        # se obtendrán del grafo de diccionario, siempre y cuando se
        # pueda encontrar el clique incluido, y se agranda (ego_graph)
        # con una distancia
        cliques = nx.algorithms.clique.find_cliques(self.G_ants)
        subgrafos = 0
        for clique in cliques:
            completo = True
            # ~ G = self.G_ants.subgraph(clique)
            G = nx.Graph()
            for nodo in clique:
                if not nodo in self.G_dict:
                    completo = False
                    break
                egoG = nx.ego_graph(self.G_dict,nodo,radius=distancia)
                G = nx.compose(G,egoG)

            if completo and nx.is_connected(G):
                centralidades = centralidad(G)
                self.grafos.append(list(G)) # No hace falta guardar todo el grafo, eso satura la RAM, basta con guardar la lista de nodos
                self.contenido.append(centralidades)
                subgrafos += 1
                print("Se han agregado " + subgrafos + " subgrafos")
                # ~ centralidades_sorted = [w for w in sorted(centralidades, key=centralidades.get, reverse=True)]

    def armaIndice(self):
        # Esta función toma el contenido que se armó en la función
        # armaContenido, y lo manipula para obtener finalmente el índice

        # El primer paso es obtener el total de terminos (nodos) que
        # puede haber en cualquiera de los grafos (vocabulario)
        vocabulario = set()
        for grafo in self.grafos:
            for nodo in grafo:
                vocabulario.add(nodo)
        # Ya con el vocabulario, puedo crear un diccionario para cada
        # término, en ese diccionario tendré el índice del grafo mapeado
        # a su score de centralidad
        self.indice = {}
        for termino in vocabulario:
            self.indice[termino] = {}
        # Solo falta entonces recorrer los grafos para alimentar el
        # diccionario
        for i, centralidades in enumerate(self.contenido):
            for termino in centralidades:
                if not centralidades[termino]: continue
                self.indice[termino][i] = centralidades[termino]

        # Lo que tengo ahora es el índice, y eso ya podría ser suficiente
        # pero creo que es mejor tener un ranking, seguramente
        # facilitará las cosas
        for termino in self.indice:
            centralidades=self.indice[termino]
            ranking = [w for w in sorted(centralidades, key=centralidades.get, reverse=True)]
            self.ranking[termino] = ranking

def main(args):
    identificador = args[1]

    G_dict = nx.read_edgelist("grafo_diccionarioRAE"+identificador+".tsv", data=(("flux",float),))
    G_ants = nx.read_edgelist("grafo"+identificador+".tsv", data=(("flux",float),))

    indice = indiceGrafos(G_dict,G_ants)
    indice.armaContenido()
    indice.armaIndice()

    with open('indiceGrafos.pickle','w') as pkfile:
        pickle.dump(indice,pkfile)

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
