#!/bin/bash

if [[ ! -d pares_por_func ]]; then mkdir pares_por_func; fi
if [[ ! -d pares_uniq ]]; then mkdir pares_uniq; fi

awk '{print $4}' respuestas.txt | sort -u |
  while read middle; do
	mid=$(printf '%q' "$middle") # Esto escapa los caracteres especiales
	lineas=$(grep -P " "$mid" [^ ]+.?$" respuestas.txt)
	echo "$lineas" | awk '{print $3,$5}' > temp1
	echo "$lineas" | awk '{print $5,$3}' > temp2
	sort -u temp1 > pares1 ; rm temp1
	sort -u temp2 > pares2 ; rm temp2
	simetricos=$(comm -12 pares1 pares2)
	rm pares1 pares2
	if [[ $simetricos ]]; then
		echo "$simetricos" > "pares_por_func/_${middle}_"
		awk '{print $1}' "pares_por_func/_${middle}_" | sort | uniq -u |
		  while read palabra; do
			pal=$(printf '%q' "$palabra")
			grep -P "^"$pal"\b" "pares_por_func/_${middle}_"
		  done | sort -u > "pares_uniq/_${middle}_"
	fi
  done



lista_archivos=$(ls -S pares_uniq/)
archivo_principal=$(echo "$lista_archivos" | head -1)
funcs_simetricas=""
archivos_antonimos=""
archivos_antonimos_por_func=""

while read archivo; do
	lineas_comunes=$(comm -12 "pares_uniq/$archivo_principal" "pares_uniq/$archivo" | wc -l)
	if [[ "$lineas_comunes" > 1 ]]; then # Este es un hyperparametro
		funcs_simetricas="${funcs_simetricas}${archivo}\n"
		archivos_antonimos="${archivos_antonimos}pares_uniq/${archivo} "
		archivos_antonimos_por_func="${archivos_antonimos_por_func}pares_por_func/${archivo} "
	else
		break
	fi

done < <(echo "$lista_archivos")

cat $archivos_antonimos | sort -u > auto_antonimos.txt
cat $archivos_antonimos_por_func | sort -u > auto_antonimos_full.txt
echo -ne "$funcs_simetricas" > funcs_simetricas.txt
