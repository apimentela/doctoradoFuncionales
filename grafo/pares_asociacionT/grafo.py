#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa necesita que en la carpeta "pares_por_func" se tengan los diferentes pares que se obtuvieron para cada una de las
# palabras funcionales que se detectaron

# Este programa asume que existe un servidor de freeling funcionando, para lograrlo es necesario instalarlo y correr un comando como este:
#   analyze --server --port 9191 -f /usr/share/freeling/config/es.cfg --flush --output json
#   analyze --server --port 9191 -f /usr/local/share/freeling/config/es.cfg --flush --output json
# La segunda opción es para mac

# Luego de correr este programa, también se obtiene un archivo tsv con las
# conexiones y sus pesos, a partir de allí se puede obtener el vocabulario
# facilmente (vocabulario que se necesita mas adelante para el proceso de los
# diccionarios. la instrucción para el vocabulario sería algo como:
#   { awk '{print $1}' grafo.tsv ; awk '{print $2}' grafo.tsv ; } | sort -u > vocabulario.txt

import os
import json
import subprocess
import pickle
import pandas as pd

carpeta = "pares_por_func"
puerto = "9191"

def obtener_tablas_por_func():
    tablas_por_func = {}
    lista_estimulos_general = []
    lista_respuestas_general = []
    lista_valores_general = []

    for nombre_archivo in os.listdir(carpeta):
        lista_estimulos = []
        lista_respuestas = []
        lista_valores = []
        with open(carpeta+"/"+nombre_archivo) as archivo:
            for linea in archivo:
                linea = " ".join(linea.split()) # Limipieza de espacios
                par = linea.split()
                std_text = par[0] + ' y ' + par[1]

                salida_comando = subprocess.check_output(('analyzer_client',puerto),input=std_text.encode()) # EL ULTIMO ENCODE ES NECESARIO
                info_freeling = json.loads(salida_comando)
                try:
                    lema1 = info_freeling["sentences"][0]["tokens"][0]["lemma"].strip()
                    lema2 = info_freeling["sentences"][0]["tokens"][2]["lemma"].strip()
                except:
                    lema1 = par[0].strip()
                    lema2 = par[1].strip()
                if lema1.isnumeric() or lema2.isnumeric() : continue
                if lema1 == lema2 : continue
                # ~ print(lema1,lema2)
                lista_estimulos.append(lema1)
                lista_respuestas.append(lema2)
                lista_valores.append(1)
                lista_estimulos_general.append(lema1)
                lista_respuestas_general.append(lema2)
                lista_valores_general.append(1)

        estimulo_respuestas = pd.DataFrame({'estimulo':lista_estimulos,'respuesta':lista_respuestas,'valor':lista_valores})

        tablas_por_func[nombre_archivo] = estimulo_respuestas

    estimulo_respuestas_generales = pd.DataFrame({'estimulo':lista_estimulos_general,'respuesta':lista_respuestas_general,'valor':lista_valores_general})

    with open('tablas_por_func.pickle','wb') as f:
        pickle.dump(tablas_por_func,f)
    with open('pares_lematizados.pickle','wb') as f:
        pickle.dump(estimulo_respuestas_generales,f)
    return tablas_por_func , estimulo_respuestas_generales

def main(args):
    # En el proceso inicial obtengo dos archivos, unas listas de pares lematizados generales (todo junto)
    #   y un pickle con tablas de los pares separados según la palabra funcional de origen
    if "tablas_por_func.pickle" not in os.listdir() or "pares_lematizados.pickle" not in os.listdir():
        tablas_por_func , tabla_general = obtener_tablas_por_func()
    else:
        with open("tablas_por_func.pickle",'rb') as f:
            tablas_por_func = pickle.load(f)
        with open("pares_lematizados.pickle",'rb') as f:
            tabla_general = pickle.load(f)

    conteos_estimulos = tabla_general.drop(columns=['respuesta']).groupby(['estimulo']).sum()
    conteos_pares = tabla_general.groupby(['estimulo','respuesta']).sum()

    conteos_estimulos["valor"] = conteos_estimulos["valor"].astype(float)
    conteos_pares["valor"] = conteos_pares["valor"].astype(float)

    # ~ for i in conteos_estimulos.index:
        # ~ print(conteos_estimulos.loc[i]["valor"])

    for i,j in conteos_pares.index:
        # ~ print (i,j,conteos_estimulos.loc[i]["valor"],conteos_pares.loc[i].loc[j]["valor"])
        conteos_pares.loc[i].loc[j]["valor"] /= conteos_estimulos.loc[i]["valor"]
        # ~ print (i,j,conteos_estimulos.loc[i]["valor"],conteos_pares.loc[i].loc[j]["valor"])

    pesos_finales = {}
    pares_registrados = set([])

    for i,j in conteos_pares.index:
        par_ordenado = sorted([i,j])
        key = "<->".join(par_ordenado)
        if key in pares_registrados: continue

        pares_registrados.add(key)

        primera = par_ordenado[0]
        segunda = par_ordenado[1]

        if primera not in pesos_finales: pesos_finales[primera] = {}
        try:
            pesos_finales[primera][segunda] = max(conteos_pares.loc[i].loc[j]["valor"],conteos_pares.loc[j].loc[i]["valor"])
        except:
            try:
                pesos_finales[primera][segunda] = conteos_pares.loc[i].loc[j]["valor"]
            except:
                try:
                    pesos_finales[primera][segunda] = conteos_pares.loc[j].loc[i]["valor"]
                except:
                    continue

    with open("grafo.tsv",'w') as archivo:
        for i in pesos_finales:
            print("======")
            print(i)
            print("--PALABRAS--")
            for j in pesos_finales[i]:
                print(j,pesos_finales[i][j])
                archivo.write(" ".join([i,j,str(pesos_finales[i][j])])+"\n")


    # ~ for nombre_archivo in os.listdir(carpeta):
        # ~ conteos_estimulos = tablas_por_func[nombre_archivo].drop(columns=['respuesta']).groupby(['estimulo']).sum()
        # ~ ## print(conteos_estimulos)
        # ~ conteos_pares = tablas_por_func[nombre_archivo].groupby(['estimulo','respuesta']).sum()
        # ~ ## print(conteos_pares)

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
