#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa trabaja con el grafo que se forma de un diccionario y con los
# grafos de parejas de oposición, el objetivo es recortar el grafo de
# diccionarios para obtener uno en el que se tengan solamente los términos mas
# importantes para la tarea de las definiciones onomasilogicas

import networkx as nx
# ~ import pickle
# ~ import signal
# ~ import sys

def main(args):
    # Este programa recibe el identificador y un archivo con los cliques
    # para procesar, obviamente para esto primero se tienen que obtener
    # los cliques, pero aún así es mejor porque la parte pesada viene despues
    identificador = args[1]
    distancia = int(args[2])
    clique_file = args[3]
    
    centralidad=nx.algorithms.betweenness_centrality
    
    G_dict = nx.read_edgelist("grafo_diccionarioRAE"+identificador+".tsv", data=(("flux",float),))
    G_ants = nx.read_edgelist("grafo"+identificador+".tsv", data=(("flux",float),))
    
    with open(clique_file) as archivo_cliques:
        for linea in archivo_cliques:
            clique = linea.split()
            completo = True
            #G = G_ants.subgraph(clique)
            G = nx.Graph()
            for nodo in clique:
                if not nodo in G_dict:
                    completo = False
                    break
                egoG = nx.ego_graph(G_dict,nodo,radius=distancia)
                G = nx.compose(G,egoG)

            if completo and nx.is_connected(G):
                centralidades = centralidad(G)
                print(centralidades)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
