#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Este programa trabaja con el programa de definiciones de la RAE, el programa se puede obtener aquí:
#	https://github.com/squat/drae
# En la página vienen las instrucciones para instalar, creo que es mejor la instalación local con go,
# para eso tuve que instalar snap. Pero para que funcione se necesita pasar el
#	ejecutable a alguna carpeta del PATH, el ejecutable se descarga en la carpeta ~/go/bin

# Este programa asume que existe un servidor de freeling funcionando, para lograrlo es necesario instalarlo y correr un comando como este:
#	analyze --server --port 9191 -f /usr/share/freeling/config/es.cfg --flush --output json
#	analyze --server --port 9191 -f /usr/local/share/freeling/config/es.cfg --flush --output json
# La segunda opción es para mac

import json
import subprocess
from nltk.corpus import stopwords

palabras_funcionales = stopwords.words("spanish")

def main(args):
	# En general, el programa debe recorrer todas las palabras que encontré en un grafo para buscar su definición. Pero debo recordar que necesita un timer porque no todas las palabras tienen definición en el diccionario, y si no encuentra definición la RAE, se queda trabada.
    identificador = args[1] # Esto es porque estoy manejando dos grafos, uno con genitiva y uno sin genitiva, asi que con esto los puedo identificar 1-con, 2-sin
    conexiones = {}
    puerto = "9191"
    pais = "México"
    comando1 = "drae"
    comando2 = "define"

    diccionario_local={}
    with open("RAE_local.json") as archivo:
        for linea in archivo:
            info = json.loads(linea)
            diccionario_local[info["palabra"]] = info["drae"]

    sinRAE=set([])
    with open("sinRAE.txt") as archivo:
        for linea in archivo:
            sinRAE.add(linea.strip())

    archivo_diccionario = open("RAE_local.json",'a')
    archivo_omisiones = open("sinRAE.txt",'a')

    with open("vocabulario"+identificador+".txt") as archivo_vocabulario:
        for palabra in archivo_vocabulario:
            palabra = palabra.strip()
            print("buscando palabra:",palabra)
            if palabra in sinRAE:
                print("NO HAY ENTRADA")
                continue
            if palabra in diccionario_local:
                info_drae = diccionario_local[palabra]
            else:
                try:
                    salida_comando = subprocess.check_output([comando1,comando2,palabra],timeout=2)
                    info_drae = json.loads(salida_comando)
                    # Este punto implica una nueva entrada que se puede agregar al
                    # diccionari local
                    nueva_entrada={}
                    nueva_entrada["palabra"]=palabra
                    nueva_entrada["drae"]=info_drae
                    print("NUEVA ENTRADA")
                    archivo_diccionario.write(json.dumps(nueva_entrada)+"\n")
                    archivo_diccionario.flush()
                except subprocess.TimeoutExpired:
                    print("NO SE ENCONTRÓ")
                    # Este punto implica que hay una palabra que no tiene
                    # entrada en la rae, puedo guardarla en el archivo para no
                    # tener que hacer la búsqueda nuevamente
                    archivo_omisiones.write(palabra+"\n")
                    archivo_omisiones.flush()
                    continue
            conexiones[palabra] = {}
            print("procesando palabra:", palabra)
            definiciones_filtradas = []
            for entrada in info_drae:
                definiciones = entrada["definitions"]
                for definicion in definiciones:
                    origen = definicion["origin"]
                    if origen and pais not in origen: continue
                    texto = definicion["definition"]
                    definiciones_filtradas.append(texto)

            ponderador = 0.0
            for definicion in definiciones_filtradas:
                salida_freeling = subprocess.check_output(('analyzer_client',puerto),input=definicion.encode()) # El enconde es necesario
                info_freeling = json.loads(salida_freeling)
                ponderador += 1
                ponderacion = 1.0/ponderador
                for oracion in info_freeling["sentences"]:
                    for token in oracion["tokens"]:
                        lemma = token["lemma"]
                        if lemma in palabras_funcionales or not lemma.isalpha(): continue
                        conexiones[palabra][lemma]=ponderacion
    with open("grafo_diccionario"+identificador+".tsv",'w') as archivo_salida:
        for i in conexiones:
            for j in conexiones[i]:
                archivo_salida.write(" ".join([i,j,str(conexiones[i][j])])+"\n")
        return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
