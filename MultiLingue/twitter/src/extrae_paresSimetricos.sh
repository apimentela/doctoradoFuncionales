#!/bin/bash

##	extrae_Simetricos
#
#	Este programa trabaja en conjunto con el de perl para extraer los pares
#	simetricos de los patrones del artículo:
#		Symmetric Pattern Based Word Embeddingsfor Improved Word Similarity Prediction

# DEPENDENCIAS:
#	extrae_Simetricos.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"
export carpeta_salida="out/pares_Simetricos/"

cd ..

patrones="X and Y
X or Y
X and the Y
from X to Y
X or the Y
X as well as Y
X or a Y
X rather than Y
X nor Y
X and one Y
either X or Y"

function llama_perl {
	archivo_entrada="$1"
	patron="$2"
	perl -C -- src/extrae_paresS.pl "$archivo_entrada" "$patron"
}
export -f llama_perl

function main {
	patron="$1"
	if [[ $flag_split == true ]]; then parallel llama_perl ::: "corpus/split_${prefijo_archivo}_out"/* ::: "$patron"
	else llama_perl "corpus/${prefijo_archivo}_out" "$patron"
	fi
}

if [[ ! -d "${carpeta_salida}" ]];then
	mkdir -p "${carpeta_salida}"
fi

while read patron; do
	main "$patron" > "${carpeta_salida}temp"
	sort -u "${carpeta_salida}temp" > "${carpeta_salida}$patron"
	awk '{print $2,$1}' "${carpeta_salida}$patron" | sort > "${carpeta_salida}temp"
	comm -12 "${carpeta_salida}$patron" "${carpeta_salida}temp" > "${carpeta_salida}${patron}_Simetrico"
done < <(echo "$patrones")

rm "${carpeta_salida}temp"
