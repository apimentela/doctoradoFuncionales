#!/usr/bin/perl

##	extrae_pares
#	Este programa tiene el propósito de obtener pares de palabras
#	relacionadas

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $palabra_estimulo = '\S+';
my $archivo_entrada = $ARGV[0];

my $lista_funcs_1 = 'la|el|los|las|del|su|un|al|se|the|san|más|una|lo|sus|dos|le';

my $func_genitiva = 'de';
my $funcs_2 = 'y|en|a|por|es|que|con|como|para|desde|era|o|hacia|hasta|fue|contra|durante|entre|sobre|son';
my $lista_funcs_2 = $func_genitiva . '|' . $funcs_2;

my $lista_funcs = $lista_funcs_1 . '|' . $lista_funcs_2;

my $ignorar = '(DIGITO|[^\w\s])';

my $expresion_1 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $lista_funcs_2 . ') (?=(?:' . $lista_funcs_1 . ') (\S+)\b)';
my $expresion_2 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $lista_funcs_2 . ') (?=\1 (\S+)\b)';
my $expresion_3 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $funcs_2 . ') (?=(?:' . $lista_funcs_1 . ') (\S+)\b)';
my $expresion_4 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $funcs_2 . ') (?=\1 (\S+)\b)';

my $verificacion = '\b(?:' . $lista_funcs_1 . '|' . $lista_funcs_2 . ')\b|DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion_2/g){
		my $palabra1 = $2;
		my $palabra2 = $4;
		my $func_mid = $3;
		if ( $palabra1 =~ /$verificacion/ || $palabra2 =~ /$verificacion/ || $palabra1 eq $palabra2 ) { next; }
		#print "$palabra1 $palabra2\n";
		print "$palabra1 $func_mid $palabra2\n"
	}
}
close INPUT;
