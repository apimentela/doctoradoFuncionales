#!/bin/bash

##	cohipos_y_antonimos
#
#	Este programa manipula las respuestas del proceso de extracción de
#	pares para dar con una lista de cohiponimos y antonimos,
#	checa la simetría de los pares para los cohiponimos
#	y tener un par único para los atónimos


nombre_programa="$BASH_SOURCE"


function main {
	awk '{print $3,$5}' respuestas.txt | sort > temp1
	awk '{print $5,$3}' respuestas.txt | sort > temp2
	comm -12 temp1 temp2 | while read par; do echo "$par" | tr " " "\n" | sort | tr "\n" " " ; echo ""; done | sort -u | sort -R > cohipos.txt

	# Antonimos
	awk '{print $1}' cohipos.txt > temp
	awk '{print $2}' cohipos.txt >> temp
	sort temp | uniq -u > uniqs
	cat uniqs | while read palabra; do grep -w "$palabra" cohipos.txt ; done | sort -u | sort -R > antonimos.txt
}

main
