#!/bin/bash

##	extrae_paresE
#
#	Este programa trabaja en conjunto con el de perl para extraer y filtrar
#	pares de palabras relacionadas entre sí. En particular este programa
#	esta diseñado para ingles

# DEPENDENCIAS:
#	extrae_paresE.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=true

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"
export carpeta_salida="out/pares_asociacionE_RC/"

cd ..

function llama_perl {
	archivo_entrada="$1"
	perl -C -- src/extrae_paresE_RC.pl "$archivo_entrada"
	#mv "${archivo_entrada}_multi" "$carpeta_salida" # este archivo lo crea perl
	mv "${archivo_entrada}_conect" "$carpeta_salida"
}
export -f llama_perl

function main {
	if [[ $flag_split == true ]]; then parallel llama_perl ::: "corpus/split_${prefijo_archivo}_out"/* 
	else llama_perl "corpus/${prefijo_archivo}_out" 
	fi > "${carpeta_salida}temp_salida"
	sort "${carpeta_salida}temp_salida" | uniq -c | sort -rn \
	 > "${carpeta_salida}respuestas"
	#~ rm "${carpeta_salida}temp_salida"
	primer1=$(grep -wnm 1 "^ *1" "${carpeta_salida}respuestas" | awk -F ":" '{print $1}')
	if [[ "$primer1" -gt 1 ]]; then
		(( primer1-- ))
		head -n "$primer1" "${carpeta_salida}respuestas" > "${carpeta_salida}temp_salida"
		mv "${carpeta_salida}temp_salida" "${carpeta_salida}respuestas"
	fi
	#cat "${carpeta_salida}"*_multi | sort | uniq -c | sort -rn \
	# > "${carpeta_salida}multi_palabras"
	#rm "${carpeta_salida}"*_multi
	cat "${carpeta_salida}"*_conect | sort | uniq -c | sort -rn \
	 > "${carpeta_salida}conectadas"
	rm "${carpeta_salida}"*_conect

	awk '{ print $2,$4 }' "${carpeta_salida}conectadas" | sort -u \
	 > "${carpeta_salida}conectadas_uniq"
	awk '{ print $2,$4 }' "${carpeta_salida}respuestas" | sort -u \
	 > "${carpeta_salida}respuestas_uniq"
}

if [[ ! -d "${carpeta_salida}" ]];then
	mkdir -p "${carpeta_salida}"
fi

main
