#!/usr/bin/perl

##	extrae_pares
#	Este programa tiene el propósito de obtener pares de palabras
#	relacionadas

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $palabra_estimulo = '\S+';
my $archivo_entrada = $ARGV[0];

# para obtener las listas, me estoy quedando con aquellas palabras de la
#	izquierda que tienen más de una palabra de la derecha asociada, una
#	vez que las encuentro, le sumo las palabras de la lista extra, y para
#	las palabras de la derecha me quedo con todas las asociadas a las de
#	la izquierda.

my $lista_funcs_1 = 'la|los|el|las|del|más|al|una|su|un|se|sus|leader|no|sin|lo|como';

my $func_genitiva = 'de';
my $funcs_2 = 'y|en|sobre|a|por|para|con|que|entre|es|o';
my $lista_funcs_2 = $func_genitiva . '|' . $funcs_2;

my $lista_funcs = $lista_funcs_1 . '|' . $lista_funcs_2;

my $ignorar = '(\d|DIGITO|[^\w\s])';

my $expresion_1 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $lista_funcs_2 . ') (?=(?:' . $lista_funcs_1 . ') (\S+)\b)';
my $expresion_2 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $lista_funcs_2 . ') (?=\1 (\S+)\b)';
my $expresion_3 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $funcs_2 . ') (?=(?:' . $lista_funcs_1 . ') (\S+)\b)';
my $expresion_4 = '\b(' . $lista_funcs_1 . ') (' . $palabra_estimulo . ') (' . $funcs_2 . ') (?=\1 (\S+)\b)';

my $verificacion = '\b(?:' . $lista_funcs_1 . '|' . $lista_funcs_2 . ')\b|\d|DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion_4/g){
		my $palabra1 = $2;
		my $palabra2 = $4;
		my $func_mid = $3;
		my $temp1 = $1;
		my $temp2 = $5;
		if ( $palabra1 =~ /$verificacion/ || $palabra2 =~ /$verificacion/ || $palabra1 eq $palabra2 || $func_mid eq "de" ) { next; }
		#print "$palabra1 $palabra2\n";
		print "$temp1 $palabra1 $func_mid $palabra2 $temp2\n"
	}
}
close INPUT;
