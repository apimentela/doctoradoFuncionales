#!/bin/bash

##	extrae_pares
#
#	Este programa trabaja en conjunto con el de perl para extraer y filtrar
#	pares de palabras relacionadas con una lista de entrada.

# DEPENDENCIAS:
#	extrae_paresT.pl

nombre_programa="$BASH_SOURCE"

# Default behavior
export flag_split=false

# Parse short options
OPTIND=1
while getopts "sj" opt
do
  case "$opt" in
	"s") flag_split=true;;
	"j") flag_split=false;;
	":") echo "La opción -$OPTARG necesita un argumento";;
	"?") echo "Opción desconocida: -$OPTARG" >&2 ; usage ; exit 1;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

export prefijo_archivo="$1"

cd ..

function llama_perl {
	archivo_entrada="$1"
	perl -C -- src/extrae_paresT.pl "$archivo_entrada"
}
export -f llama_perl

function main {
	if [[ $flag_split == true ]]; then parallel llama_perl ::: "corpus/split_${prefijo_archivo}_out"/* 
	else llama_perl "corpus/${prefijo_archivo}_out" 
	fi > "out/pares_asociacionT/temp_salida"
	sort "out/pares_asociacionT/temp_salida" | uniq -c | sort -rn \
	 > "out/pares_asociacionT/respuestas.txt"
	rm "out/pares_asociacionT/temp_salida"
}

if [[ ! -d "out/pares_asociacionT/" ]];then
	mkdir -p "out/pares_asociacionT/"
fi

main
