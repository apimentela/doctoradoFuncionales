#!/usr/bin/perl

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $archivo_entrada = $ARGV[0];

my $expresion = '\b(\S+) (?=(\S+) (\S+) (\S+) (\S+)\b)';

my $verificacion = 'DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion/g){
		my $primero = $1;
		my $segundo = $2;
		my $tercero = $3;
		my $cuarto = $4;
		my $quinto = $5;
		if ( $_ =~ /$verificacion/ ) { next; }
		$primero =~ s/ /_/g;
		$segundo =~ s/ /_/g;
		$tercero =~ s/ /_/g;
		$cuarto =~ s/ /_/g;
		$quinto =~ s/ /_/g;
		print "$primero $segundo $tercero $cuarto $quinto\n";	
	}	
}
close INPUT;

