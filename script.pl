#!/usr/bin/perl

# Se usan estas dos líneas para que pueda leer sin problemas los parámetros como utf8
use Encode qw(decode_utf8);
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my $archivo_entrada = $ARGV[0];

my $lista = 'the|a|his|one|two|her|new|their|its|he|she|they|be|on|and|by|about|or|that|for|to|at|as|is|in|of|with|was|are';

#my $expresion = '\b((?:' . $lista .') \S+) (?=((?:' . $lista . ') \S+ (?:' . $lista . '))\b)';
my $expresion = '\b((?:(?:' . $lista . ')\b *?)+) ((?:(?:\S+) *?)+?) (?=((?:(?:' . $lista . ')\b *?)+) ((?:(?:\S+) *?)+?) ((?:(?:' . $lista . ')\b *?)+)\b)';

my $verificacion = '\b(?:' . $lista . ')\b|DIGITO|[^\w\s]';

open(INPUT,"<$archivo_entrada") or die "No se pudo abrir el archivo, $!";
while(<INPUT>){
	while ($_ =~ /$expresion/g){
		my $primero = $1;
		my $segundo = $2;
		my $tercero = $3;
		my $cuarto = $4;
		my $quinto = $5;
		if ( $segundo =~ /$verificacion/ or $cuarto =~ /$verificacion/ or $segundo eq $cuarto ) { next; }
		$primero =~ s/ /_/g;
		$segundo =~ s/ /_/g;
		$tercero =~ s/ /_/g;
		$cuarto =~ s/ /_/g;
		$quinto =~ s/ /_/g;
		print "$primero $segundo $tercero $cuarto $quinto\n";	
	}	
}
close INPUT;

