#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  gui.py
#  
#  Copyright 2018 pimentel <pimentel@ELC13XS000A058U>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import tkinter
import re

class App:
	def __init__(self,master):
		self.master=master
		
		self.instrucciones=tkinter.Label(self.master, justify="left",text="INSTRUCCIONES DE USO:\n- j : Co-hipónimo aceptable\n- f : NO se acepta como co-hipónimo\n- r : Regresar\n- n : Dejar para depués\n\n")
		self.instrucciones.pack()

		self.label=tkinter.Label(self.master,text=" PALABRA1 - PALABRA2 ", font=("Courier",44))
		self.label.pack()
		
		self.label.bind("<Key>", self.key)
		self.label.bind("<Button-1>", self.click)

		self.label.focus_set()
		self.label.pack()
		
	def key(self, event):
		print("tecla", repr(event.char))
		self.label.configure(text="cambiado")

	def click(self, event):
		self.label.focus_set()
		print("clicked at:", event.x, event.y)

if __name__ == '__main__':
	import sys
	import os
	
	init_dir=os.getcwd() 
	window = tkinter.Tk()
	#~ window.title("Co-hipónimos")
	app= App(window)
	
	window.mainloop()
	
	
	#~ sys.exit(main(sys.argv))
